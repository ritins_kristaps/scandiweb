<?php require_once('../private/initialize.php'); ?>

<?php $page_title = 'Add Product'; ?>
<?php include(SHARED_PATH . '/public_header.php'); ?>

<?php include(SHARED_PATH . '/public_form.php'); ?>

<?php include(SHARED_PATH . '/public_footer.php'); ?>
