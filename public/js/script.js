var type = $("#selectInput");

$(type).change(function () {
    if (type.val() === "dvd") {
        dvd();
    } else if (type.val() === "book") {
        book();
    } else if (type.val() === "furniture") {
        furniture();
    } else if (type.val() === "") {
        switcher();
    }
});


function dvd() {
    $(".size").removeClass("hidden");
    $(".weight").addClass("hidden");
    $(".furniture").addClass("hidden");
    $("#sizeInput").prop('required', true);
}

function book() {
    $(".size").addClass("hidden");
    $(".weight").removeClass("hidden");
    $(".furniture").addClass("hidden");
    $("#weightInput").prop('required', true);
}

function furniture() {
    $(".size").addClass("hidden");
    $(".weight").addClass("hidden");
    $(".furniture").removeClass("hidden");
    $("#heightInput").prop('required', true);
    $("#widthInput").prop('required', true);
    $("#lengthInput").prop('required', true);
}

function switcher() {
    $(".size").addClass("hidden");
    $(".weight").addClass("hidden");
    $(".furniture").addClass("hidden");
}





