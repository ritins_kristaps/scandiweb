<?php

require_once('../../private/initialize.php');

if (is_post_request()) {
    $args = $_POST['product'];

    $product = new Product($args);
    $result = $product->create();


    redirect_to("../product-list.php");
}