<?php require_once('../private/initialize.php'); ?>

<?php $page_title = 'Product List'; ?>
<?php include(SHARED_PATH . '/public_header.php'); ?>


<?php

$books = Book::find_all();
$dvds = Disc::find_all();
$pofurniture = Furniture::find_all();

?>

<div class="container">
<h1>Product List</h1>
<?php foreach ($books as $book) { ?>
    <div class="box">
    <input type="checkbox" class="checkbox" name="ids[]">
    <p><?= $book->get_data() . "<br>"; ?></p>
    </div>
<?php 
} ?>

<?php foreach ($dvds as $dvd) { ?>
    <div class="box">
    <input type="checkbox" class="checkbox" name="ids[]">
    <p><?= $dvd->get_data() . "<br>"; ?></p>
    </div>
<?php 
} ?>

<?php foreach ($pofurniture as $furniture) { ?>
    <div class="box">
    <input type="checkbox" class="checkbox" name="ids[]">
    <p><?= $furniture->get_data() . "<br>"; ?></p>
    </div>
<?php 
} ?>
</div>


<?php include(SHARED_PATH . '/public_footer.php'); ?>
