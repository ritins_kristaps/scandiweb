<?php

class Furniture extends Product
{
    static public function find_all()
    {
        $sql = "SELECT * FROM products WHERE height != ''";
        $result = self::$database->query($sql);
      //Result into object
        $object_array = [];
        while ($record = $result->fetch_assoc()) {
            $object_array[] = self::instantiate($record);
        }
        $result->free();
        return $object_array;
    }

    static protected function instantiate($record)
    {
        $object = new self;
        foreach ($record as $property => $value) {
            if (property_exists($object, $property)) {
                $object->$property = $value;
            }
        }
        return $object;
    }

    public function get_data()
    {
        $data = $this->sku . "<br>";
        $data .= $this->name . "<br>";
        $data .= $this->get_price() . "<br>";
        $data .= "Dimension: " . $this->height . "x" . $this->width . "x" . $this->length;
        return $data;
    }
}