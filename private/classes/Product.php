<?php

class Product
{
// Start of active record code
  static protected $database;

  static protected $db_columns = ["sku", "name", "price", "size", "weight", "width", "length", "height"];

  static public function set_database($database)
  {
    self::$database = $database;
  }

  public function create()
  {
    $attributes = $this->sanitazed_attributes();
    $sql = "INSERT INTO products (";
    $sql .= join(", ", array_keys($attributes));
    $sql .= ") VALUES ('";
    $sql .= join("', '", array_values($attributes));
    $sql .= "')";

    $result = self::$database->query($sql);
    return $result;
  }

  public function attributes()
  {
    $attributes = [];
    foreach (self::$db_columns as $column) {
      $attributes[$column] = $this->$column;
    }
    return $attributes;
  }

  protected function sanitazed_attributes()
  {
    $sanitazed = [];
    foreach ($this->attributes() as $key => $value) {
      $sanitazed[$key] = self::$database->escape_string($value);
    }
    return $sanitazed;
  }

  public function delete()
  {
    $sql = "DELETE FROM products";
    $result = self::$database->query($sql);
    return $result;
  }


// End of active record code
  public $id;
  public $sku;
  public $name;
  public $price;
  public $size;
  public $weight;
  public $width;
  public $length;
  public $height;

  public function __construct($args = [])
  {
    $this->sku = $args['sku'] ?? null;
    $this->name = $args['name'] ?? null;
    $this->price = $args['price'] ?? null;
    $this->size = $args['size'] ?? null;
    $this->weight = $args['weight'] ?? null;
    $this->width = $args['width'] ?? null;
    $this->height = $args['height'] ?? null;
    $this->length = $args['length'] ?? null;
  }

  public function get_price()
  {
    return number_format($this->price, 2) . " $";
  }

}


?>
