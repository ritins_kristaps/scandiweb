<div class="container">
    <div class="product-add">
        <h1>Add Product</h1>
    </div>
    
<form method="POST" action="products/new.php">

<div class="form-group row">
    <label for="skuInput" class="col-sm-1 col-form-label">SKU</label>
    <div class="col-sm-3">
        <input name="product[sku]" type="text" class="form-control" id="skuInput" aria-describedby="skuHelp" required>
    </div>
</div>

<div class="form-group row">
    <label for="nameInput" class="col-sm-1 col-form-label">Name</label>
    <div class="col-sm-3">
        <input name="product[name]" type="text" class="form-control" id="nameInput" required>
    </div>
</div>

<div class="form-group row">
    <label for="priceInput" class="col-sm-1 col-form-label">Price</label>
    <div class="col-sm-3">
        <input name="product[price]" type="text" class="form-control" id="priceInput" required>
    </div>
</div>

<div class="form-group row">

    <label for="selectInput" class="col-sm-1 col-form-label">Type Switcher</label>

    <div class="col-sm-3">
        <select type="text" class="form-control" id="selectInput" required>
            <option selected value="">Type Switcher</option>
            <option value="dvd">CD</option>
            <option value="book">Books</option>
            <option value="furniture">Furniture</option>
        </select>
    </div>
</div>

<div class="form-group row hidden size">
    <label for="sizeInput" class="col-sm-1 col-form-label">Size</label>
    <div class="col-sm-3">
        <input name="product[size]" type="text" class="form-control" id="sizeInput">
        <small id="sizeInput" class="form-text text-muted">Provide the size of the CD (MB)</small>
    </div>
</div>

<div class="form-group row hidden weight">
    <label for="weightInput" class="col-sm-1 col-form-label">Weight</label>
    <div class="col-sm-3">
        <input name="product[weight]" type="text" class="form-control" id="weightInput">
        <small id="weightInput" class="form-text text-muted">Provide the weight of the product</small>
    </div>
</div>

<div class="hidden furniture">
    <div class="form-group row">
        <label for="heightInput" class="col-sm-1 col-form-label">Height</label>
        <div class="col-sm-3">
            <input name="product[height]" type="text" class="form-control" id="heightInput">
        </div>
    </div>

    <div class="form-group row">
        <label for="widthInput" class="col-sm-1 col-form-label">Width</label>
        <div class="col-sm-3">
            <input name="product[width]" type="text" class="form-control" id="widthInput">
        </div>
    </div>

    <div class="form-group row">
        <label for="lengthInput" class="col-sm-1 col-form-label">Length</label>
        <div class="col-sm-3">
            <input name="product[length]" type="text" class="form-control" id="lengthInput">         
            <small id="weightInput" class="form-text text-muted">Please provide dimensions in H x W x L format</small>
        </div>
    </div>
</div>


<button type="submit" id="save-button" class="btn btn-primary">SAVE</button>

</form>

</div>